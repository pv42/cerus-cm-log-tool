use std::{
    collections::HashMap,
    fs::{self, File},
    io::{self, BufWriter, Read, Write},
    process,
};

use anyhow::Result;
use chrono::{DateTime, Duration, FixedOffset, TimeDelta, Utc};
use console::{style, Color, Style};
use elite_insight_json::{
    dps_report_api::get_ei_data_cached,
    ei_dto::{
        ei_actor::EIActor,
        ei_data_types::EliteInsightData,
        ei_player::{DamageSource, DamageType, EIPlayer, Profession},
    },
    log_manager::{LMEncounter, LMEncounterMode, LogManagerCache},
};
use indicatif::{ProgressBar, ProgressStyle};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use serde::{Deserialize, Serialize};
use sysinfo::{self, Pid, System};

const SIGNET_OF_ILLUSIONS: i64 = 10247;
const PORTAL_ENTRE: i64 = 10197;
const PORTAL_EXEUNT: i64 = 10199;
//const FEEDBACK: i64 = 10302;
const CERUS_EMPOWERED: i64 = 69550;
const MALICIOUIS_SHADOW_ID: i32 = 25645;

const CONFIG_FILE: &str = "config.toml";

fn default_cache_path() -> String {
    "logs_cache/".to_owned()
}

fn default_ignore_malicious_shadow_dps() -> bool {
    false
}

fn default_max_age_seconds() -> i64 {
    -1
}

fn default_print_to_stdout() -> bool {
    true
}

#[derive(Deserialize)]
struct Config {
    #[serde(default = "default_cache_path")]
    cache_path: String,
    #[serde(default = "default_ignore_malicious_shadow_dps")]
    ignore_malicious_shadow_dps: bool,
    #[serde(default = "default_max_age_seconds")]
    max_log_age_seconds: i64,
    #[serde(default = "default_print_to_stdout")]
    print_to_stdout: bool,
    #[serde(default)]
    has_player: Option<String>,
}

fn load_config_file() -> Result<Config> {
    let contents = fs::read_to_string(CONFIG_FILE)?;
    toml::from_str(&contents).map_err(|err| err.into())
}

#[derive(Serialize, Clone)]
struct CerusCMLogData {
    log_url: String,
    duration: f64,
    start_time: DateTime<FixedOffset>,
    final_hp_percent: f64,
    players: Vec<CerusCMPlayer>,
    phases: Vec<CerusCMPhase>,
}

#[derive(Serialize, Clone)]
struct CerusCMPhase {
    name: String,
    duration: f64,
    final_empowered_stacks: i32,
}

#[derive(Serialize, Clone)]
struct CerusCMPlayer {
    name: String,
    build: CerusCMBuild,
    orbs_collected: u32,
    orbs_collected_empowered: u32,
    targeted_by_malice: u32,
    avg_dps: i32,
    phase_dps: Vec<i32>,
    hit_by_rage: u32,
}

fn get_config() -> Config {
    match load_config_file() {
        Ok(cfg) => cfg,
        Err(_) => Config {
            cache_path: "logs_cache/".to_owned(),
            ignore_malicious_shadow_dps: false,
            max_log_age_seconds: -1,
            print_to_stdout: true,
            has_player: None,
        },
    }
}

#[derive(Debug, Serialize, Clone, Copy, PartialEq, Eq, Hash)]
enum CerusCMRole {
    DPS,
    UtilityVirt,
    QDPS,
    HealAlac,
    // placeholder for other roles
    Other,
}

#[derive(Debug, Serialize, Clone, Copy)]
enum CerusCMBuild {
    Druid,
    Scourge,
    DPSVirt,
    PortalVirt,
    OtherVirt,
    CondiQuickHerald,
    PowerQuickHerald,
    Daredevil,
    Specter,
    Tempest,
    Scrapper,
    CondiWillbender,
    Chrono,
    Harbinger,
    Firebrand,
    // placeholder for other builds
    Other,
}

impl CerusCMBuild {
    fn get_color(&self) -> Color {
        match self {
            CerusCMBuild::Druid => Color::Color256(10),
            CerusCMBuild::Scourge => Color::Color256(2),
            CerusCMBuild::Harbinger => Color::Color256(2),
            CerusCMBuild::DPSVirt => Color::Color256(5),
            CerusCMBuild::PortalVirt => Color::Color256(13),
            CerusCMBuild::OtherVirt => Color::Color256(13),
            CerusCMBuild::CondiQuickHerald => Color::Color256(9),
            CerusCMBuild::PowerQuickHerald => Color::Color256(9),
            CerusCMBuild::Daredevil => Color::Color256(1),
            CerusCMBuild::Specter => Color::Color256(1),
            CerusCMBuild::Tempest => Color::Red,
            CerusCMBuild::Other => Color::White,
            CerusCMBuild::Scrapper => Color::Yellow,
            CerusCMBuild::CondiWillbender => Color::Cyan,
            CerusCMBuild::Firebrand => Color::Cyan,
            CerusCMBuild::Chrono => Color::Color256(13),
        }
    }

    fn get_role(&self) -> CerusCMRole {
        match self {
            CerusCMBuild::Druid | CerusCMBuild::Scourge => CerusCMRole::HealAlac,
            CerusCMBuild::DPSVirt
            | CerusCMBuild::CondiWillbender
            | CerusCMBuild::Tempest
            | CerusCMBuild::Daredevil
            | CerusCMBuild::Specter => CerusCMRole::DPS,
            CerusCMBuild::PortalVirt | CerusCMBuild::OtherVirt => CerusCMRole::UtilityVirt,
            CerusCMBuild::CondiQuickHerald | CerusCMBuild::PowerQuickHerald => CerusCMRole::QDPS,
            _ => CerusCMRole::Other,
        }
    }
}

fn get_build(player: &EIPlayer) -> CerusCMBuild {
    match player.profession {
        Profession::Virtuoso => {
            if let Some(skills) = player.get_skills_used() {
                for skill in skills.iter() {
                    match skill.id {
                        PORTAL_ENTRE | PORTAL_EXEUNT => return CerusCMBuild::PortalVirt,
                        SIGNET_OF_ILLUSIONS => return CerusCMBuild::DPSVirt,
                        _ => (),
                    }
                }
            }
            CerusCMBuild::OtherVirt
        }
        Profession::Druid => CerusCMBuild::Druid,
        Profession::Scourge => CerusCMBuild::Scourge,
        Profession::Tempest => CerusCMBuild::Tempest,
        Profession::Herald => {
            if player.get_total_dps(DamageType::Condi, DamageSource::All)
                > player.get_total_dps(DamageType::Power, DamageSource::All)
            {
                CerusCMBuild::CondiQuickHerald
            } else {
                CerusCMBuild::PowerQuickHerald
            }
        }
        Profession::Daredevil => CerusCMBuild::Daredevil,
        Profession::Scrapper => CerusCMBuild::Scrapper,
        Profession::Specter => CerusCMBuild::Specter,
        Profession::Willbender
            if player.get_total_dps(DamageType::Condi, DamageSource::All)
                > player.get_total_dps(DamageType::Power, DamageSource::All) =>
        {
            CerusCMBuild::CondiWillbender
        }
        Profession::Chronomancer => CerusCMBuild::Chrono,
        Profession::Harbinger => CerusCMBuild::Harbinger,
        Profession::Firebrand => CerusCMBuild::Firebrand,
        _ => {
            println!("Unknown build for player {}", player.name());
            CerusCMBuild::Other
        }
    }
}

fn get_logs(config: &Config) -> Result<Vec<(String, EliteInsightData)>> {
    let lmc = LogManagerCache::get_system_cache()?;
    let count = lmc.logs_by_filename.len();
    let bar = ProgressBar::new(count as u64);
    bar.set_style(
        ProgressStyle::with_template("[{elapsed:>3}] {msg} [{bar:40.white}] {pos:>7}/{len:7}")
            .unwrap()
            .progress_chars("=> "),
    );
    bar.set_message(style("Loading from LMC").cyan().bold().to_string());
    let now = Utc::now();
    let logs = lmc
        .logs_by_filename
        .par_iter()
        .filter_map(|(_file, lm_log)| {
            if lm_log.encounter != LMEncounter::Cerus
                || lm_log.dps_report_ei_upload.url.is_none()
                || lm_log.encounter_mode == LMEncounterMode::Normal
                || ((now - lm_log.encounter_start_time.to_utc()).num_seconds()
                    > config.max_log_age_seconds
                    && config.max_log_age_seconds > 0)
                || (config.has_player.is_some()
                    && lm_log.players.is_some()
                    && lm_log
                        .players
                        .clone()
                        .unwrap()
                        .iter()
                        .find(|p| p.name == config.has_player.clone().unwrap())
                        .is_none())
            {
                bar.inc(1);
                return None;
            }
            let url = lm_log.dps_report_ei_upload.url.clone().unwrap();
            match get_ei_data_cached(&url, Some(&config.cache_path)) {
                None => {
                    println!("Err: failed to get '{url}'");
                    bar.inc(1);
                    None
                }
                Some(data) => {
                    if !data.is_cm
                        || data
                            .targets
                            .get(0)
                            .map(|t| t.health_percent_burned() < 5.0)
                            .unwrap_or(true)
                    {
                        bar.inc(1);
                        None
                    } else {
                        Some((url, data))
                    }
                }
            }
        })
        .collect();
    bar.set_message(style("Loaded from LMC").green().bold().to_string());
    bar.finish();
    Ok(logs)
}

fn write_to_json(data: &[CerusCMLogData]) -> Result<()> {
    let filename = "cerus-data.json";
    println!("Creating {}", filename);
    let file = File::create(filename)?;
    let mut writer = BufWriter::new(file);
    let str = serde_json::to_string_pretty(&data)?;
    writer.write_all(str.as_bytes())?;
    Ok(())
}

fn fmt_duration(seconds: f64) -> String {
    format!("{}:{:04.1}", seconds as i32 / 60, seconds % 60.0)
}

fn write_to_html(
    data: &[CerusCMLogData],
    per_player_stats: &Vec<(String, StatsPerPlayer)>,
    per_role_dps: &HashMap<CerusCMRole, Vec<i32>>,
) -> Result<()> {
    let filename = "cerus-cm.htm";
    println!("Creating {}", filename);
    let mut html = r#"<!DOCTYPE html><html data-bs-theme="dark" lang="en"><head>
        <title>Cerus CM</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    </head>
    <body><div class="container">
    <canvas id="dpsChart"></canvas>
    <canvas id="hpChart"></canvas>"#.to_string();
    html += r#"<div class="accordion" id="accordionLogs">"#;
    for log in data {
        let collapse_id = log.log_url.clone();
        html += r#"<div class="accordion-item">"#;
        html += r#"<h2 class="accordion-header">"#;
        html += &format!(
            r##"<button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#{collapse_id}" aria-expanded="true" aria-controls="{collapse_id}">"##
        );
        html += &format!(r#"<h5>{} "#, log.start_time.to_utc());
        html += &format!(
            r#"<span class="badge text-bg-secondary">{:.1}%</span> "#,
            log.final_hp_percent
        );
        html += &format!(
            r#"<span class="badge text-bg-secondary">{}</span> "#,
            fmt_duration(log.duration)
        );
        html += &format!(
            r#"<a target="_blank" href={} class="btn btn-secondary btn-sm"><i class="bi bi-box-arrow-up-right"></i></a></h5>"#,
            log.log_url
        );
        html += "</button></h2>";
        html += &format!(
            r##"<div id="{collapse_id}" class="accordion-collapse collapse" data-bs-parent="#accordionLogs">"##
        );
        html += r#"<div class="accordion-body">"#;
        html += r#"<table class="table"><tr><th>Name</th><th>Build</th><th>Orbs</th><th>Orbs empowered</th><th>DPS</th>"#;
        for phase in log.phases.iter() {
            html += &format!("<th>{}</th>", phase.name);
        }
        html += "</tr>";
        for player in log.players.iter() {
            html += &format!(
                "<tr><td>{}</td><td>{:?}</td><td>{}</td><td>{}</td><td>{}</td>",
                player.name, player.build, player.orbs_collected, player.orbs_collected_empowered, player.avg_dps
            );
            for (index, phase_dps) in player.phase_dps.iter().enumerate() {
                let dps_cmp = per_role_dps
                    .get(&player.build.get_role())
                    .map(|v| v.get(index))
                    .unwrap_or(None);
                match dps_cmp {
                    Some(dps_cmp) => {
                        html += &format!(
                            "<td>{} [{:.0}%]</td>",
                            phase_dps,
                            *phase_dps as f64 / *dps_cmp as f64 * 100.0
                        );
                    }
                    None => html += &format!("<td>{}</td>", phase_dps),
                }
            }
            html += "</tr>";
        }
        html += &format!("<tr><td>Phase duration</td><td/><td/><td/>");
        for phase in log.phases.iter() {
            html += &format!("<td>{}</td>", fmt_duration(phase.duration));
        }
        html += "</tr>";
        html += &format!("<tr><td>Empowered Stacks at end of phase</td><td/><td/><td/>");
        for phase in log.phases.iter() {
            html += &format!("<td>{}</td>", phase.final_empowered_stacks);
        }
        html += "</tr>";
        html += "</table>\n";
        html += "</div></div></div>";
    }
    html += "</div>";
    html += "<h5>Player statistics</h5>";
    html += r#"<table class="table"><tr><th>Player</th><th>#Logs</th><th>Average % of orbs collected</th><th>Average # of empowered orbs collected</th><th>Average DPS</th><th>Average % of malious intent gained</th><th>Hit by Rage</th></tr>"#;
    for (player, pp) in per_player_stats.iter() {
        html += &format!(
            "<tr><td>{player}</td><td>{}</td><td>{:.1}</td><td>{:.1}</td><td>{}</td><td>{:.1}</td><td>{}</td></tr>",
            pp.count,
            pp.avg_orbs * 100.0,
            pp.avg_orbs_p3,
            pp.avg_dps,
            pp.avg_malice * 100.0,
            pp.rage_hits
        );
    }
    html += "</table>";
    html += "<h5>Average DPS per Role</h5>";
    html += r#"<table class="table"><tr><th>Role</th><th>Phase 1</th><th>Split 1</th><th>Phase 2</th><th>Split 2</th></tr>"#;
    for (role, v) in per_role_dps.iter() {
        if role == &CerusCMRole::Other {
            continue;
        }
        html += &format!("<tr><td>{role:?}</td>");
        for dps in v {
            html += &format!("<td>{dps}</td>");
        }
        html += &format!("</tr>");
    }
    html += "</table>";
    html += "</div>";
    let final_hp: Vec<_> = data.iter().map(|log| (log.final_hp_percent * 100.0).round() / 100.0).collect();
    let best_final_hp: Vec<_> = final_hp
        .iter()
        .scan(100.0, |best, this| {
            if this < best {
                *best = *this
            };
            Some(*best)
        })
        .collect();
    let group_dps: Vec<_> = data
        .iter()
        .map(|log| log.players.iter().map(|p| p.avg_dps).sum::<i32>())
        .collect();
    let mut dps_per_phase: HashMap<String, Vec<String>> = HashMap::new();
    let mut phase_order = vec![];
    for (log_index, log) in data.iter().enumerate() {
        for (phase_index, phase) in log.phases.iter().enumerate() {
            let sum: i32 = log
                .players
                .iter()
                .map(|p| p.phase_dps.get(phase_index).unwrap())
                .sum();
            if !dps_per_phase.contains_key(&phase.name) {
                phase_order.push(phase.name.clone());
            }
            let v = dps_per_phase.entry(phase.name.clone()).or_insert(vec![]);
            while v.len() < log_index {
                v.push("null".to_owned())
            }
            v.push(sum.to_string())
        }
    }
    let labels: Vec<_> = data
        .iter()
        .map(|log| format!("{}", log.start_time.to_utc()))
        .collect();
    let mut per_phase_str = String::new();
    for phase_name in phase_order {
        let data: &Vec<String> = dps_per_phase.get(&phase_name).unwrap();
        per_phase_str += &format!(
            "{{
            label: '{phase_name}',
            data: {data:?},
            borderWidth: 1
        }},",
        );
    }
    html += &format!(
        r#"<script>
    const hpctx = document.getElementById('hpChart');
  
    new Chart(hpctx, {{
      type: 'line',
      data: {{
        labels: {:?},
        datasets: [{{
          label: 'Final HP',
          data: {:?},
          borderWidth: 1
        }},
        {{
            label: 'Best Attempt',
            data: {:?},
            borderWidth: 1
        }}]
      }},
      options: {{
        scales: {{
          y: {{
            min: 0,
            reverse: true,
            max: 100,
            title: {{
                display: true,
                text: 'Remaining HP %'
            }}
          }},
          x: {{
            display: true,
            ticks: {{
                display: false
            }},
            title: {{
                display: true,
                text: 'Attempts'
            }}
          }}
        }},
        plugins: {{
            tooltip: {{
                callbacks: {{
                    label: function(ctx) {{
                        let label = ctx.dataset.label || '';
                        if (ctx.parsed.y !== null) {{
                            label +=  ': ' + ctx.parsed.y + '%';
                        }}
                        return label;
                    }} 
                }}
            }}
        }}
      }}
    }});

    const dpsctx = document.getElementById('dpsChart');
  
    new Chart(dpsctx, {{
      type: 'line',
      data: {{
        labels: {:?},
        datasets: [{{
          label: 'Group DPS',
          data: {:?},
          borderWidth: 1
        }},
        {}
        ]
      }},
      options: {{
        scales: {{
          y: {{
            beginAtZero: true,
            title: {{
                display: true,
                text: 'Group DPS'
            }}
          }},
          x: {{
            display: true,
            ticks: {{
                display: false
            }},
            title: {{
                display: true,
                text: 'Attempts'
            }}
          }}
        }}
      }}
    }});
</script>"#,
        labels, final_hp, best_final_hp, labels, group_dps, per_phase_str
    );
    html += r#"<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>"#;
    html += "</body></html>";

    let file = File::create(filename)?;
    let mut writer = BufWriter::new(file);
    writer.write_all(html.as_bytes())?;
    Ok(())
}

fn print_to_terminal(data: &CerusCMLogData) {
    println!("URL      : {}", data.log_url);
    println!("Duration : {:.0}s", data.duration as f64);
    println!("Final HP : {}%", data.final_hp_percent);
    println!("Players:");
    print!(" Name                 Build               Orbs OrbsEm Avg DPS    ");
    for phase in data.phases.iter() {
        print!("{:7}  ", phase.name);
    }
    println!();

    let mut total_orbs = 0;
    let mut total_orbs_p3 = 0;
    let mut total_dps: i32 = 0;
    let mut total_phases_dps = vec![0; data.phases.len()];

    for player in data.players.iter() {
        let style = Style::new().fg(player.build.get_color()).bold();
        let build_name = format!("{:?}", player.build);
        print!(
            " {:20} {:20}{:4}   {:4}   {:-4.1}k  ",
            player.name,
            style.apply_to(build_name),
            player.orbs_collected,
            player.orbs_collected_empowered,
            player.avg_dps as f64 / 1000.0
        );
        total_orbs += player.orbs_collected;
        total_orbs_p3 += player.orbs_collected_empowered;
        total_dps += player.avg_dps;
        for (index, phase_dps) in player.phase_dps.iter().enumerate() {
            total_phases_dps[index] += phase_dps;
            print!("    {:-4.1}k", *phase_dps as f64 / 1000.0);
        }
        println!()
    }
    print!(
        " {:20} {:20}{:4}   {:4}  {:-5.0}k  ",
        "Total DPS",
        " ",
        total_orbs,
        total_orbs_p3,
        total_dps as f64 / 1000.0
    );
    for phase_dps in total_phases_dps {
        print!("   {:-5.0}k", phase_dps as f64 / 1000.0);
    }
    println!();
    print!(
        " {:20} {:20}{:4}  {:4}    {:-5.0}  ",
        "Phase Duration", "", "", "", ""
    );
    for phase in data.phases.iter() {
        print!("   {:-5.0}s", phase.duration);
    }
    println!();

    print!(
        " {:19} {:21}               {:-4}  ",
        "Empowered Stacks at",
        "end of phase",
        data.phases
            .last()
            .map(|phase| phase.final_empowered_stacks)
            .unwrap_or_default()
    );
    for phase in data.phases.iter() {
        print!("     {:-4}", phase.final_empowered_stacks);
    }
    println!();
    println!();
}

fn get_player_dps(player: &EIPlayer, target_indices: &[usize]) -> i32 {
    target_indices
        .iter()
        .map(|i| player.get_target_dps(*i))
        .sum()
}

fn data_from_log(
    url: String,
    log: &EliteInsightData,
    ignore_malicious_shadow_dps: bool,
) -> CerusCMLogData {
    let dps_phase_indices_names_durations: Vec<_> = log
        .phases
        .iter()
        .enumerate()
        .filter_map(|(index, phase)| {
            if phase.breakbar_phase || index == 0 {
                None
            } else {
                Some((
                    index,
                    phase.name.clone(),
                    (phase.get_end() - phase.get_start()).num_milliseconds() as f64 / 1000.0,
                    log.targets
                        .first()
                        .map(|target| {
                            target.get_buff_count_at_time(
                                CERUS_EMPOWERED,
                                phase.get_end(),
                            )
                        })
                        .unwrap_or_default(),
                ))
            }
        })
        .collect();

    let mut used_targets = vec![];
    for (index, target) in log.targets.iter().enumerate() {
        if !(target.id() == MALICIOUIS_SHADOW_ID && ignore_malicious_shadow_dps) {
            used_targets.push(index);
        }
    }

    let mut players_sorted = log.players.clone().to_vec();
    players_sorted.sort_by(|p1, p2| {
        get_player_dps(p2, &used_targets).cmp(&get_player_dps(p1, &used_targets))
    });

    let mut player_data = vec![];

    let orbs_collected = log.get_mechanic_counts("Ins.A", None);
    let mut empowered_orbs_collected: HashMap<String, u32> = Default::default();
    let phase2_end = log.phases.iter().find(|p| p.name == "Phase 2").map(|p| p.get_end()).unwrap_or(TimeDelta::max_value());
    let glutony_kill = log.get_mechanic_by_name("Emp.Gluttony.K").map(|m| m.mechanics_data.first().map(|ev| ev.get_time())).unwrap_or(None).unwrap_or(Duration::max_value());
    let emp_orbs: Vec<_> = log.get_mechanic_by_name("Ins.A").map(|md| md.mechanics_data.into_iter()
        .filter(|m| m.get_time() > phase2_end && m.get_time() < glutony_kill).collect()
    ).unwrap_or(vec![]);
    for phase3_instance in emp_orbs {
        empowered_orbs_collected.entry(phase3_instance.actor.clone()).and_modify(|v| *v += 1).or_insert(1);
    }
    let malices = log.get_mechanic_counts("MalInt.A", None);
    let rages = log.get_mechanic_counts("CryRage.H", None);
    let emp_rages = log.get_mechanic_counts("Emp.CryRage.H", None);
    for player in players_sorted.iter() {
        let build = get_build(player);
        let dps = get_player_dps(player, &used_targets);
        let orbs = *orbs_collected.get(player.name()).unwrap_or(&0);
        let orbs_empowered = *empowered_orbs_collected.get(player.name()).unwrap_or(&0);
        let malice = *malices.get(player.name()).unwrap_or(&0);
        let rage =
            *rages.get(player.name()).unwrap_or(&0) + *emp_rages.get(player.name()).unwrap_or(&0);
        let phases_dps: Vec<i32> = dps_phase_indices_names_durations
            .iter()
            .map(|(index, _, _, _)| {
                player.get_dps_phase(DamageType::Total, DamageSource::All, *index)
            })
            .collect();

        player_data.push(CerusCMPlayer {
            name: player.name().to_string(),
            build,
            orbs_collected: orbs,
            orbs_collected_empowered: orbs_empowered,
            targeted_by_malice: malice,
            avg_dps: dps,
            hit_by_rage: rage,
            phase_dps: phases_dps,
        })
    }

    let log_data = CerusCMLogData {
        log_url: url.clone(),
        duration: log.duration.num_milliseconds() as f64 / 1000.0,
        final_hp_percent: 100.0
            - log
                .targets
                .first()
                .map(|target| target.health_percent_burned())
                .unwrap_or_default(),
        players: player_data,
        start_time: log.time_start_std,
        phases: dps_phase_indices_names_durations
            .iter()
            .map(|(_index, name, duration, stacks)| CerusCMPhase {
                name: name.clone(),
                duration: *duration,
                final_empowered_stacks: *stacks,
            })
            .collect(),
    };
    log_data
}

fn collect_data(
    logs: Vec<(String, EliteInsightData)>,
    ignore_malicious_shadow_dps: bool,
) -> Vec<CerusCMLogData> {
    let count = logs.len();
    let bar = ProgressBar::new(count as u64);
    bar.set_style(
        ProgressStyle::with_template("[{elapsed:>3}] {msg} [{bar:40.white}] {pos:>7}/{len:7}")
            .unwrap()
            .progress_chars("=> "),
    );
    bar.set_message(style("Processing logs").cyan().bold().to_string());

    let data = logs
        .par_iter()
        .map(|(url, log)| {
            let d = data_from_log(url.clone(), log, ignore_malicious_shadow_dps);
            bar.inc(1);
            d
        })
        .collect();
    bar.set_message(style("Processed logs ").green().bold().to_string());
    bar.finish();
    data
}

struct StatsPerPlayer {
    avg_orbs: f64,
    avg_orbs_p3: f64,
    avg_malice: f64,
    avg_dps: i32,
    count: i32,
    rage_hits: u32,
}

fn get_avg_per_player(data: &[CerusCMLogData]) -> Vec<(String, StatsPerPlayer)> {
    let mut total_count_orbs: HashMap<String, u32> = HashMap::new();
    let mut orbs_player: HashMap<String, u32> = HashMap::new();
    let mut orbs_p3_player: HashMap<String, u32> = HashMap::new();
    let mut dps_sum: HashMap<String, i32> = HashMap::new();
    let mut attendence_count: HashMap<String, i32> = HashMap::new();
    let mut totals_malice: HashMap<String, u32> = HashMap::new();
    let mut player_malice: HashMap<String, u32> = HashMap::new();
    let mut rage: HashMap<String, u32> = HashMap::new();

    for log in data.iter() {
        let log_orbs: u32 = log.players.iter().map(|p| p.orbs_collected).sum();
        let log_malices: u32 = log.players.iter().map(|p| p.targeted_by_malice).sum();
        for player in log.players.iter() {
            *total_count_orbs.entry(player.name.clone()).or_default() += log_orbs;
            *orbs_player.entry(player.name.clone()).or_default() += player.orbs_collected;
            *orbs_p3_player.entry(player.name.clone()).or_default() += player.orbs_collected_empowered;
            *dps_sum.entry(player.name.clone()).or_default() += player.avg_dps;
            *attendence_count.entry(player.name.clone()).or_default() += 1;
            *player_malice.entry(player.name.clone()).or_default() += player.targeted_by_malice;
            *totals_malice.entry(player.name.clone()).or_default() += log_malices;
            *rage.entry(player.name.clone()).or_default() += player.hit_by_rage;
        }
    }
    let mut avg_player: Vec<_> = total_count_orbs
        .into_iter()
        .map(|(name, count)| {
            (
                name.clone(),
                StatsPerPlayer {
                    avg_orbs: *orbs_player.get(&name).unwrap_or(&0) as f64 / count as f64,
                    avg_orbs_p3: (*orbs_p3_player.get(&name).unwrap_or(&0) as f64 / *attendence_count.get(&name).unwrap_or(&1) as f64),
                    avg_malice: *player_malice.get(&name).unwrap_or(&0) as f64
                        / *totals_malice.get(&name).unwrap_or(&1) as f64,
                    avg_dps: (*dps_sum.get(&name).unwrap_or(&0) as f64
                        / *attendence_count.get(&name).unwrap_or(&1) as f64)
                        as i32,
                    count: *attendence_count.get(&name).unwrap_or(&0),
                    rage_hits: *rage.get(&name).unwrap_or(&0),
                },
            )
        })
        .collect();
    avg_player.sort_by(|(_, v1), (_, v2)| v2.count.cmp(&v1.count));
    for (player, avg) in avg_player.iter() {
        println!("{player:19} - {:-4.1}%", avg.avg_orbs * 100.0);
    }
    avg_player
}

fn get_avg_per_build_and_phase(logs: &[CerusCMLogData]) -> HashMap<CerusCMRole, Vec<i32>> {
    let mut dps_data: HashMap<CerusCMRole, Vec<(i32, i32)>> = HashMap::new();
    for log in logs.iter() {
        let ph_len = log.phases.iter().count();
        for phase_index in 0..ph_len - 1 {
            for player in log.players.iter() {
                let v = dps_data.entry(player.build.get_role()).or_insert(vec![]);
                while v.len() <= phase_index {
                    v.push((0, 0));
                }
                v[phase_index].0 += player.phase_dps.get(phase_index).unwrap();
                v[phase_index].1 += 1
            }
        }
    }
    let mut avg: HashMap<CerusCMRole, Vec<i32>> = HashMap::new();
    for (role, v) in dps_data {
        let entry = avg.entry(role).or_insert(vec![]);
        for (sum, count) in v {
            entry.push(sum / count);
        }
    }
    println!("AVG DPS per Role and Phase");
    // todo
    let phase_names = vec!["Phase 1", "Split 1", "Phase 2", "Split 2", "Phase 3", ""];
    for (role, role_data) in avg.iter() {
        for (phase, value) in role_data.iter().enumerate() {
            let role = format!("{role:?}");
            println!("{role:11}  {}  {value:5}", phase_names[phase]);
        }
    }
    avg
}

fn is_in_terminal() -> bool {
    // wait for exit if not run from cargo, cmd or powershell
    let mut system = System::new();
    let pid = Pid::from_u32(process::id());
    system.refresh_process(pid);
    let ppid = system.process(pid).unwrap().parent().unwrap();
    system.refresh_process(ppid);
    let pname = system.process(ppid).unwrap().name();
    match pname {
        "cargo.exe" | "cmd.exe" | "powershell.exe" => true,
        _ => false,
    }
}

fn main() {
    println!("Loading logs ...");
    let is_term = is_in_terminal();
    let config = get_config();
    if let Err(err) = fs::create_dir_all(&config.cache_path) {
        println!(
            "Failed to create cache path \"{}\": {err}",
            &config.cache_path
        );
    } else {
        match get_logs(&config) {
            Ok(mut logs) => {
                logs.sort_by(|(_, log1), (_, log2)| log1.time_start_std.cmp(&log2.time_start_std));
                println!("Found {} Cerus CM or LM logs", logs.len());
                let data = collect_data(logs, config.ignore_malicious_shadow_dps);
                if config.print_to_stdout {
                    for log in data.iter() {
                        print_to_terminal(&log);
                    }
                }
                println!("Average portion of orbs collected by each player");
                let avg_orbs = get_avg_per_player(&data);
                println!();
                let role_avg = get_avg_per_build_and_phase(&data);
                println!();
                if let Err(err) = write_to_json(&data) {
                    println!("Failed to generate JSON output: {err}")
                };
                if let Err(err) = write_to_html(&data, &avg_orbs, &role_avg) {
                    println!("Failed to generate HTML output: {err}")
                }
            }
            Err(err) => println!("Failed to read logs: {err}"),
        }
    }
    if !is_term {
        println!("Press Enter to exit ...");
        let _ = io::stdin().read(&mut []);
    }
}
