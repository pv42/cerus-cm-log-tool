# Cerus CM Log Tool
Simple tool to extract and display information about Guild Wars 2's Strike Temple of Febe for Challenge and Legendary Challenge mode arcdps logs.
## Usage
Download the latest executable from [here](https://gitlab.com/api/v4/projects/56143931/jobs/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/cerus-cm.exe?job=build).

The program can either be run as is or from a terminal (cmd or powershell).

The tool will analyze all logs that are:
- either Cerus CM or Cerus LM,
- uploaded using [arcdps Log Manager](https://gw2scratch.com/tools/manager) **Version 1.11** or newer and 
- have less then 95% Cerus HP remaining

For all logs the detailed data will be downloaded from dps.report and analyzed. The download is cached so subsequent executions will not download old data again.

The data will be outputed into:
- the application's terminal window
- as HTML in cerus-cm.htm (open in a browser)
- as machine readable JSON in cerus-data.json

The following data will be generated:
- fight's duration
- Cerus final HP
- each player's build (only some builds are supported)
- (big) orbs collected by each player
- (big) empowered collected by each player, these are the orbs that give a permanent debuff (in embodiment of sin or during the 50% phase)
- (cleave) dps per player for the whole fight aswell as for each phase
- duration of each phase
- number of empowered stacks at the end of each phase

## Developer Notes
To build the executable:
- install Rust: https://www.rust-lang.org/tools/install
- clone this repo: `git clone https://gitlab.com/pv42/cerus-cm-log-tool.git`
- build using cargo: `cargo build --release`
- the executable will be in `target/release/` as `cerus-cm.exe`

## Configuration
Configuration can be done by creating a `config.toml` file next to the executable. 
The following configuration options are supported:
| Name                        | Type   | Default       | Description                                                                  |
| --------------------------- | ------ | ------------- | ---------------------------------------------------------------------------- |
| cache_path                  | string | 'logs_cache/' | Which path to use for caching downloaded log data                            |
| ignore_malicious_shadow_dps | bool   | false         | Whether to ignore the Malicious shadows when calculating dps                 |  
| max_log_age_seconds         | int    | -1            | Maximum age of logs to be considered, or negative to include all logs        |
| print_to_stdout             | bool   | true          | Wether to print the results to stdout (e.g. the terminal)                    |
| has_player                  | string | -             | Specify a player name that must be present for the log to be considered, leave unset to skip this filter |

An example config file could look like this: 
```toml
cache_path = "D:/GW2LogsData/logJSONs/"
ignore_malicious_shadow_dps = true
max_log_age_seconds = 14400
```

## Acknowledgements
This tool depends heavly on the work of other people and would not work without their amazing work:
- [arcdps logmanager](https://gw2scratch.com/tools/manager) by Sejsel for getting the logs that need to be analized
- [dps.report](https://dps.report) by Micca for storing the log data
- [Elite Insights](https://github.com/baaron4/GW2-Elite-Insights-Parser) by baaron, EliphasNUIT and Linka for parsing the logs and generating the the json out this relies on
- and of course [arcdps](https://www.deltaconnected.com/arcdps/) by deltaconnected for generating the log files
